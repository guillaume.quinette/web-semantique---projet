import edu.emory.mathcs.nlp.decode.AbstractNLPDecoder;
import edu.emory.mathcs.nlp.decode.NLPDecoder;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

/**
 * Classe "wrapper" pour l'utilisation de NLPDecoder.
 */
public class NLP
{
    NLPDecoder decoder;

    /**
     * Constructeur de la classe.
     */
    public NLP()
    {
        String config = "<configuration>\n" +
                        "    <tsv>\n" +
                        "        <column index=\"0\" field=\"form\"/>\n" +
                        "    </tsv>\n" +
                        "    <models>\n" +
                        "        <pos>en-pos.xz</pos>\n" +
                        "        <ner>en-ner.xz</ner>\n" +
                        "        <dep>en-dep.xz</dep>\n" +
                        "    </models>\n" +
                        "</configuration>";
        InputStream config_stream = new ByteArrayInputStream(config.getBytes());
        this.decoder      = new NLPDecoder(config_stream);
    }

    /**
     * Utilise NLPDecoder pour décoder la chaîne de caractères donnée en paramètre.
     *
     * @param str La chaîne de caractères à décoder.
     * @return La chaîne de caractères décodée.
     */
    public String decode(String str)
    {
        return this.decoder.decode(str,AbstractNLPDecoder.FORMAT_RAW);
    }

}

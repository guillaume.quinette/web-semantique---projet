import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;

import java.util.Arrays;

/**
 * Classe traitant les requêtes vers l'endpoint DBpedia.
 */
public class Request
{
    String service,prefixes;

    /**
     * Constructeur de la classe.
     */
    public Request()
    {
        this.service = "http://dbpedia.org/sparql";

        this.prefixes = "PREFIX dbo:<http://dbpedia.org/ontology/>"
                + "PREFIX dbp:<http://dbpedia.org/property/>"
                + "PREFIX dbr:<http://dbpedia.org/resource/>"
                + "PREFIX dbc:<http://dbpedia.org/resource/Category:>"
                + "PREFIX : <http://dbpedia.org/resource/>"
                + "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#/>"
                + "PREFIX foaf:  <http://xmlns.com/foaf/0.1/>"
                + "PREFIX owl: <http://www.w3.org/2002/07/owl#>";
    }

    /**
     * Utilise Apache Jena pour faire une requête vers l'endpoint DBpedia.
     *
     * @param rq La requête à envoyer.
     */
    public void sendRequest(String rq)
    {
        String[] cols = this.splitColumns(rq);


        String query = this.prefixes
                     + rq;
        //System.out.println(rq);

        QueryExecution qe = QueryExecutionFactory.sparqlService(this.service, query);
        ResultSet rs = qe.execSelect();
        while (rs.hasNext()){
            System.out.println("-------------------------------------------");

            QuerySolution s= rs.nextSolution();

            for(String col : cols)
            {
                try
                {
                    System.out.println(s.getResource(col).toString());
                }

                catch (Exception e)
                {
                    try
                    {
                        System.out.println(s.getLiteral(col));
                    }

                    catch (Exception eb){ }
                }
            }
        }
    }

    /**
     * Renvoie un tableau des colonnes à afficher en fonction de la requête.
     *
     * @param rq La requête SPARQL à traiter.
     * @return Le tableau de colonnes à afficher.
     */
    public String[] splitColumns(String rq)
    {
        return rq.split(" where")[0].split("select distinct ")[1].split(" ");
    }
}

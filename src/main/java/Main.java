
/**
 * Classe principale du programme.
 */
public class Main {
    /**
     * Fonction principale du programme.
     *
     * @param args Les arguments potentiels donnés en entrée.
     */
    public static void main(String[] args)
    {
        QuestionTranslator qt = new QuestionTranslator();
        qt.getUserQuestion();
    }
}
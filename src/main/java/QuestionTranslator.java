import edu.emory.mathcs.nlp.decode.NLPDecoder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

/**
 * Classe traitant la saisie de l'utilisateur dans sa globalité.
 */
public class QuestionTranslator
{
    private NLP n;

    /**
     * Constructeur de la classe.
     */
    public QuestionTranslator()
    {
        this.n = new NLP();
    }

    /**
     * Traite la saisie de l'utilisateur.
     */
    public void getUserQuestion(){
        Scanner in = new Scanner(System.in);
        System.out.println("--------------------------------------------------");
        System.out.println("Hi ! How could I help you ? (type \"exit\" to end the program)");
        String userInput = in.nextLine();
        if(userInput.equals("exit"))
        {
            System.exit(0);
        }
        else
        {
            processQuestion(userInput);
            getUserQuestion();
        }
    }

    /**
     * Utilise NLP pour analyser syntaxiquement la question et traduit celle-ci en requête.
     *
     * @param userInput La saisie de l'utilisateur.
     */
    public void processQuestion(String userInput)
    {
        String posTag = this.n.decode(userInput);
        String[] pos = posTag.split("\\r?\\n");
        ArrayList<String[]> tags = new ArrayList<>();
        for(int i = 0; i < pos.length; i++){
            tags.add(pos[i].split("\t"));
        }
        showArray(tags);
        translate(tags, userInput);
    }

    /**
     * Traduit la question de l'utilisateur en requête SPARQL.
     *
     * @param tags La requête à traduire.
     * @param userInput La saisie de l'utilisateur.
     */
    public void translate(ArrayList<String[]> tags, String userInput)
    {
        if (userInput.toLowerCase().contains("who's") || userInput.toLowerCase().contains("who is")){
            String namedEntity="";
            //Question is of type: Who's X/ What's Y
            for(int i = 0; i < tags.size(); i++){
                if(tags.get(i)[8].equals("B-PERSON") || tags.get(i)[8].equals("U-PERSON")){
                    namedEntity += tags.get(i)[1];
                }
                if(tags.get(i)[8].equals("L-PERSON")){
                    namedEntity += " "+tags.get(i)[1];
                }
            }
            if(namedEntity==""){
                System.out.println("No entity has been recognized...");
            }
            else {
                Request rq = new Request();
                rq.sendRequest("select distinct ?p ?o where{?p a dbo:Person . ?p foaf:name ?name filter(regex(?name, \"" + namedEntity + "\", \"i\" )) . ?p dbp:occupation ?o} LIMIT 10");
            }
        }

        else if (userInput.toLowerCase().contains("where's") || userInput.toLowerCase().contains("where is")) {
            String entity = "";
            //Question is of type: Where's X ?
            for (int i = 0; i < tags.size(); i++) {
                if (tags.get(i)[8].equals("B-FAC") || tags.get(i)[8].equals("B-ORG") || tags.get(i)[8].equals("B-PERSON") || tags.get(i)[8].equals("U-PERSON") && tags.get(i)[1] != "Where".toLowerCase()) {
                    entity += tags.get(i)[1];
                }
                if (tags.get(i)[8].equals("L-FAC") || tags.get(i)[8].equals("I-FAC") || tags.get(i)[8].equals("L-ORG") || tags.get(i)[8].equals("I-ORG") || tags.get(i)[8].equals("I-PERSON")) {
                    entity += " " + tags.get(i)[1];
                }
            }
            //System.out.println("Entité : " + entity);
            if (entity == "") {
                System.out.println("No entity has been recognized...");
            } else {
                Request rq = new Request();
                rq.sendRequest("select distinct ?p where{?p a dbo:Place . ?p foaf:name ?name filter(regex(?name, \""+entity+"\", \"i\" ))}");
            }
        }
        else if (userInput.toLowerCase().contains("what's") || userInput.toLowerCase().contains("what is")) {
            String entity = "";
            //Question is of type: What's X ?
            for (int i = 0; i < tags.size(); i++) {
                if (tags.get(i)[8].equals("B-FAC") || tags.get(i)[8].equals("B-ORG") || tags.get(i)[8].equals("B-PERSON") || tags.get(i)[8].equals("U-PERSON") || tags.get(i)[8].equals("U-GPE") || tags.get(i)[8].equals("B-GPE")) {
                    entity += tags.get(i)[1];
                }
                if (tags.get(i)[8].equals("L-FAC") || tags.get(i)[8].equals("I-FAC") || tags.get(i)[8].equals("L-ORG") || tags.get(i)[8].equals("I-ORG") || tags.get(i)[8].equals("I-PERSON") || tags.get(i)[8].equals("I-GPE") || tags.get(i)[8].equals("L-GPE")) {
                    entity += " " + tags.get(i)[1];
                }
            }
            //System.out.println("Entité : " + entity);
            if (entity == "") {
                System.out.println("No entity has been recognized...");
            } else {
                Request rq = new Request();
                rq.sendRequest("select distinct ?p where{?p a owl:Thing . ?p foaf:name ?name filter(regex(?name, \""+entity+"\", \"i\" ))}");
            }
        }
    }

    /**
     * Permet un affichage en console simple d'une ArrayList contenant des chaînes de caractères.
     *
     * @param arrayList L'ArrayList à afficher.
     */
    public void showArray(ArrayList<String[]> arrayList)
    {
        for(int i = 0; i < arrayList.size(); i++){
            System.out.println((Arrays.toString(arrayList.get(i))));
        }
    }
}
